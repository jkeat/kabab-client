import Mixin from '@ember/object/mixin';
import { observer } from '@ember/object';
import tauCharts from 'npm:taucharts';
import 'npm:taucharts/dist/plugins/tooltip';
import _ from 'npm:lodash';

export default Mixin.create({
  model: null,

  chartContainerId: null,
  chartInstance: null,

  init() {
    this._super();
    this.set('chartContainerId', _.uniqueId('chart_'));
  },

  didInsertElement() {
    this._super();
    this.drawChart();
  },

  redrawObserver: observer('model.fields.@each.tableIndex', function () {
    const chartInstance = this.get('chartInstance');

    if (chartInstance) { chartInstance.destroy(); }

    this.drawChart();
  }),

  getChartData() {
    return this.get('model.table.tauRows');
  },

  drawChart() {
    // TODO: Show a preview of the empty chart, somehow.
    if (!this.get('model.isChartable')) {
      // Early exit if it isn't drawable
      return;
    }

    const chartFields = this.getChartFields();
    const data = this.getChartData();

    const chart = new tauCharts.Chart(_.merge(chartFields, { data }, {
      type: this.get('tauChartType'),

      guide: this.getGuide(),

      settings: {
        fitModel: 'entire-view',
      },

      plugins: [
        tauCharts.api.plugins.get('tooltip')(),
      ],
    }));

    this.set('chartInstance', chart);

    chart.renderTo(`#${this.get('chartContainerId')}`);
  },
});
