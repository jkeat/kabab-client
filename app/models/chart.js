import EmberObject, { computed } from '@ember/object';
import FieldModel from 'kabab/models/field';
import chartTypes from 'kabab/utils/chart-types';
import _ from 'npm:lodash';

export default EmberObject.extend({
  /**
   * One of the [chartTypes].chartType values from utils/chart-types 
   */
  chartType: null,

  /**
   * TableModel
   */
  table: null,

  /**
   * Chart definition from kabab/utils/chart-types
   */
  chartTypeObject: computed('chartType', function () {
    return _.find(chartTypes, { chartType: this.get('chartType') });
  }),

  /**
   * Display name of the chart type
   */
  chartTypeName: computed.alias('chartTypeObject.chartTypeName'),

  /**
   * Array of of chart fields (FieldModel instances)
   * Observes all possible field index keys to avoid dynamically observing.
   * But it only maps the relevant fields into the array.
   */
  fields: computed('chartType', function () {
    return _.map(this.get('chartTypeObject.fields'), (field) =>
      FieldModel.create({
        chart: this,
        fieldDefinition: field,
      })
    );
  }),

  /**
   * Whether the required fields are filled out
   */
  isChartable: computed('fields.@each.needsFilling', function () {
    const someFieldNeedsFilling = _.chain(this.get('fields'))
      .map((field) => field.get('needsFilling'))
      .some((needsFilling) => !!needsFilling)
      .value();

    return !someFieldNeedsFilling;
  }),

  /**
   * Retrieve a field via its field key (defined in kabab/utils/chart-types)
   */
  getField(fieldKey) {
    return _.find(this.get('fields'), (field) => {
      return fieldKey === field.get('fieldKey');
    });
  },
});
