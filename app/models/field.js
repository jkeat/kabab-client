import EmberObject, { computed } from '@ember/object';
import _ from 'npm:lodash';

export default EmberObject.extend({
  /**
   * The parent ChartModel
   */
  chart: null,

  /**
   * A field definition from 'kabab/utils/chart-types'
   */
  fieldDefinition: null,

  /**
   * The table column index for this field's values
   */
  tableIndex: -1,

  /**
   * The key for this field in the parent chart's computed 'fields' object
   */
  fieldKey: computed.alias('fieldDefinition.fieldKey'),

  /**
   * The display name of this field
   */
  name: computed.alias('fieldDefinition.name'),

  /**
   * Whether the field is required to build the chart
   */
  isRequired: computed.alias('fieldDefinition.isRequired'),

  /**
   * Whether the field is filled
   */
  isFilled: computed('tableIndex', function () {
    const tableIndex = this.get('tableIndex');
    return _.isNumber(tableIndex) && tableIndex >= 0;
  }),

  /**
   * Whether the field must be filled before building the chart
   */
  needsFilling: computed('isRequired', 'isFilled', function () {
    if (!this.get('isRequired')) { return false; }

    return !this.get('isFilled');
  }),

  /**
   * The column key to the parent table's tauRows (keyed by stringified indices)
   * 
   * TODO: maybe move this inside the tau chart mixin, just like TableModel's tauRows field
   */
  tauTableKey: computed('tableIndex', 'isFilled', function () {
    if (!this.get('isFilled')) { return null; }
    return this.get('tableIndex').toString();
  }),

  /**
   * The possible colHeaders to fill this field with 
   */
  options: computed('chart.table.colHeaders.@each.name', function () {
    return this.get('chart.table.colHeaders');
  }),

  /**
   * The field's currently selected colHeader
   */
  selectedOption: computed('tableIndex', 'chart.table.colHeaders.@each.name', function () {
    return this.get('chart.table.colHeaders')[this.get('tableIndex')];
  }),
});
