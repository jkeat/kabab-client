import EmberObject, { computed } from '@ember/object';
import _ from 'npm:lodash';

export default EmberObject.extend({
  /**
   * Table's server-generated ID
   */
  id: null,

  /**
   * The header objects defining col meta data
   * 
   * TODO: make these models? probably a good idea once you add type setting
   */
  colHeaders: null,

  /**
   * Array of arrays of cell vals
   */
  rows: null,

  /**
   * Table name, scraped server side
   */
  name: null,

  /**
   * Table color, generated server side
   */
  color: null,

  /**
   * Each row is an object keyed by its column index
   *
   * TODO: move to tau chart mixin as function?
   * TODO: do it on the fly, b/c Tau removes rows with any missing cells
   */
  tauRows: computed('rows.@each.length', function () {
    const rows = this.get('rows');

    const tauRows = _.map(rows, (row) => {
      const idxKeys = _.range(row.length);
      const idxKeyStrings = _.map(idxKeys, (key) => key.toString());

      // TODO: this is sloopy;
      const numRow = _.map(row, (cell) => parseInt(cell) ? parseInt(cell) : cell);

      return _.zipObject(idxKeyStrings, numRow);
    });

    return tauRows;
  }),
});
