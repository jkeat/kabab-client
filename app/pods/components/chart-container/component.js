import Component from '@ember/component';
import { computed } from '@ember/object';

const chartTypeComponentMap = {
  scatter_plot: 'charts/scatter-plot',
  line_chart: 'charts/line-chart',
  bar_graph: 'charts/bar-graph',
};

export default Component.extend({
  classNames: ['chart-container'],

  model: null,

  chartComponentName: computed('model.chartType', function () {
    const type = this.get('model.chartType');
    return chartTypeComponentMap[type] || 'charts/this-should-explode';
  }),
});
