import Component from '@ember/component';

export default Component.extend({
  classNames: ['chart-manager'],

  model: null,

  isExpanded: false,

  actions: {
    toggleExpanded() {
      this.toggleProperty('isExpanded');
    },

    open() {
      this.set('isExpanded', true);
    },

    close() {
      this.set('isExpanded', false);
    },

    selectFieldOption(field, selectedOptionTableIndex) {
      this.get('setFieldTableIndex')(field, selectedOptionTableIndex);
    },
  },
});
