import Component from '@ember/component';
import chartTypes from 'kabab/utils/chart-types';

export default Component.extend({
  classNames: ['chart-select'],

  // current chart
  model: null,

  chartTypes: null,

  init() {
    this._super();

    this.set('chartTypes', chartTypes);
  },
});
