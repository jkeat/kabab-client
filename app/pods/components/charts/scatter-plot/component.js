import Component from '@ember/component';
import TauChartComponentMixin from 'kabab/mixins/tau-chart-component';

export default Component.extend(TauChartComponentMixin, {
  classNames: ['scatter-plot'],

  tauChartType: 'scatterplot',

  getChartFields() {
    // TODO: this sloopy
    const xField = this.get('model').getField('x');
    const yField = this.get('model').getField('y');
    const sizeField = this.get('model').getField('size');
    const colorField = this.get('model').getField('color');

    return {
      x: xField ? xField.get('tauTableKey') : null,
      y: yField ? yField.get('tauTableKey') : null,
      size: sizeField ? sizeField.get('tauTableKey') : null,
      color: colorField ? colorField.get('tauTableKey') : null,
    };
  },

  getGuide() {
    return {
      showGridLines: '',
      x: {
        label: {
          text: this.get('model').getField('x').get('selectedOption.name'),
        },
        padding: 50,
        nice: false,
      },
      y: {
        label: {
          text: this.get('model').getField('y').get('selectedOption.name'),
        },
        nice: false,
      },
    };
  },
});
