import Component from '@ember/component';
import Split from 'npm:split.js';
import _ from 'npm:lodash';

export default Component.extend({
  classNames: ['split-panels'],

  component1: null,
  component2: null,

  panel1id: null,
  panel2id: null,

  init() {
    this._super();

    this.setProperties({
      panel1id: _.uniqueId('panel_'),
      panel2id: _.uniqueId('panel_'),
    });
  },

  didInsertElement() {
    this._super();

    Split([`#${this.get('panel1id')}`, `#${this.get('panel2id')}`], {
      sizes: [65, 35],
      minSize: 250,
      direction: 'vertical',
      snapOffset: 0,
    });
  },
});
