import Component from '@ember/component';
import { computed } from '@ember/object';
import _ from 'npm:lodash';

export default Component.extend({
  classNames: ['table-preview'],

  model: null,

  numCols: computed.alias('model.colHeaders.length'),

  numHiddenRows: computed('model.rows.length', function () {
    return this.get('model.rows.length') - 2;
  }),

  firstRow: computed('model.rows', function () {
    return _.first(this.get('model.rows'));
  }),

  lastRow: computed('model.rows', function () {
    return _.last(this.get('model.rows'));
  }),
});
