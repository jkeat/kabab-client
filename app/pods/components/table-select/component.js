import Component from '@ember/component';

export default Component.extend({
  classNames: ['table-select'],

  isOpen: false,

  actions: {
    toggleOpen() {
      this.toggleProperty('isOpen');
    },

    open() {
      this.set('isOpen', true);
    },

    close() {
      this.set('isOpen', false);
    },

    selectTable(table) {
      this.get('choose')(table);
      this.send('close');
    },
  },
});
