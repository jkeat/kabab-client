import Component from '@ember/component';

export default Component.extend({
  classNames: ['text-input'],

  // action
  go: null,

  val: '',

  autofocus: false,
});
