import Controller from '@ember/controller';

const suggestedLinks = [{
    url: 'https://en.wikipedia.org/wiki/List_of_tallest_buildings',
    label: 'Tallest Buildings',
  }, {
    url: 'https://en.wikipedia.org/wiki/Economy_of_the_United_States',
    label: 'United States Economy',
  }, {
    url: 'https://en.wikipedia.org/wiki/50_Greatest_Players_in_NBA_History',
    label: 'NBA Players',
}];

export default Controller.extend({
  inputUrl: '',

  suggestedLinks: null,

  init() {
    this._super();

    this.set('suggestedLinks', suggestedLinks);
  },

  actions: {
    searchUrl(url) {
      this.transitionToRoute('sandbox', url);
    },
  },
});
