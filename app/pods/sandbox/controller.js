import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default Controller.extend({
  sandbox: service(),

  actions: {
    searchUrl(url) {
      this.transitionToRoute('sandbox', url);
    },

    setActiveTable(table) {
      this.get('sandbox.chart').set('table', table);
      this.set('sandbox.activeTable', table);
    },

    setChartType(chartType) {
      this.set('sandbox.chartType', chartType);
    },

    setFieldTableIndex(field, tableIndex) {
      field.set('tableIndex', tableIndex);
    },
  },
});
