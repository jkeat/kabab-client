import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({
  sandbox: service(),

  /**
   * Fetches data, but doesn't return main model directly
   * Just ensures the service has everything ready to go
   */
  model(params) {
    const url = params.url;

    return this.get('sandbox').fetchData(url)
      .then(() => ({
        url: url,
      }));
  },

  resetController() {
    this._super(...arguments);

    this.get('sandbox').reset();
  },
});
