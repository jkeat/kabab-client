import Service from '@ember/service';
import { observer } from '@ember/object';
import ENV from 'kabab/config/environment';
import TableModel from 'kabab/models/table';
import ChartModel from 'kabab/models/chart';
import _ from 'npm:lodash';
import fetch from 'fetch';

export default Service.extend({
  tables: null,
  activeTable: null,

  chartType: null,
  chart: null,

  /**
   * Create a fresh chart whenever the chartType of activeTable changes
   * TODO: should this be a computed property?
   */
  chartTypeObserver: observer('chartType', 'activeTable.id', function () {
    if (!this.get('chartType') || !this.get('activeTable')) {
      // Early exit if we're trying to make chart with no chartType or activeTable
      // Happens when reset() is called and sets nulls
      return;
    }

    const chartModel = ChartModel.create({
      chartType: this.get('chartType'),
      table: this.get('activeTable'),
    });

    this.set('chart', chartModel);
  }),

  /**
   * Reset this no good ol' singleton
   * Basically resetController()
   * 
   * TODO: should most of this service be in the controller
   *       and/or a wrapper component?
   */
  reset() {
    this.setProperties({
      tables: null,
      activeTable: null,
      chartType: null,
      chart: null,
    });
  },

  /**
   * Fetch the data and stick it into models
   */
  fetchData(url) {
    const apiUrl = `${ENV.APP.apiBase}/parse?url=${url}`;
    return fetch(apiUrl)
      .then(res => res.json())
      .then(data => {
        // TODO: error resolution
        
        const tableData = _.get(data, 'tables');
        const tableModels = _.map(tableData, (tableData) => TableModel.create(tableData));

        this.setProperties({
          tables: tableModels,
          activeTable: tableModels.get('firstObject'),
        });

        // Trigger the chart creation by setting its type
        // TODO: I think there's a better way
        //       E.g. {{#if activeTable}} -> show chart display component
        this.set('chartType', 'scatter_plot');
      });    
  },
});
