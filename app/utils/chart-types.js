const chartTypes = [
  {
    chartType: 'scatter_plot',
    chartTypeName: 'Scatter plot',
    fields: [
      {
        fieldKey: 'x',
        name: 'x-axis',
        isRequired: true,
      }, {
        fieldKey: 'y',
        name: 'y-axis',
        isRequired: true,
      }, {
        fieldKey: 'size',
        name: 'size',
        isRequired: false,
      }, {
        fieldKey: 'color',
        name: 'color',
        isRequired: false,
      },
    ],
  }, {
    chartType: 'line_chart',
    chartTypeName: 'Line chart',
    fields: [
      {
        fieldKey: 'x',
        name: 'x-axis',
        isRequired: true,
      }, {
        fieldKey: 'y',
        name: 'y-axis',
        isRequired: true,
      }, {
        fieldKey: 'color',
        name: 'group',
        isRequired: false,
      }, {
        fieldKey: 'size',
        name: 'thickness',
        isRequired: false,
      },
    ],
  }, {
    chartType: 'bar_graph',
    chartTypeName: 'Bar graph',
    fields: [
      {
        fieldKey: 'x',
        name: 'x-axis',
        isRequired: true,
      }, {
        fieldKey: 'y',
        name: 'y-axis',
        isRequired: true,
      }, {
        fieldKey: 'color',
        name: 'color',
        isRequired: false,
      },
    ],
  },
];

export default chartTypes;
