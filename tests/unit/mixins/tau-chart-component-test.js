import EmberObject from '@ember/object';
import TauChartComponentMixin from 'kabab/mixins/tau-chart-component';
import { module, test } from 'qunit';

module('Unit | Mixin | chart component');

// Replace this with your real tests.
test('it works', function(assert) {
  let TauChartComponentObject = EmberObject.extend(TauChartComponentMixin);
  let subject = TauChartComponentObject.create();
  assert.ok(subject);
});
